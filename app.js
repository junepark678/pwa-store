import { download, Profile, fetchIcon, escape, isurl } from './util.js'

let installMedium = localStorage.getItem('pwastore_mode') || 'pwa'

const db = new Dexie('pwastore')
window._db = db
db.version(1).stores({
  sources: 'id, name'
})

function matchSearch(pwa, search) {
  search = search.toLowerCase()
  return search.includes(pwa.name.toLowerCase()) || pwa.name.toLowerCase().includes(search) ||
    search.includes(pwa.description.toLowerCase()) || pwa.description.toLowerCase().includes(search) ||
    pwa.id.toLowerCase() == search || pwa.uuid.toLowerCase() == search
}

async function buildAppProfile(source, pwa) {
  const p = new Profile(source.id + '.' + pwa.id, pwa.name, pwa.description, 1, 'PWA Store', pwa.uuid)
  p.webClip(pwa.name, pwa.url, await fetchIcon(pwa.icon), 1, pwa.uuid)

  return p
}

const SVG_VERIFIED = `<svg width="16" height="16" viewBox="0 0 64 64"><g><ellipse ry="28" rx="28" cy="32" cx="32" fill="#42c942"/><path d="m12,30.14813l4.2981,-4.29901l11.77427,11.77246l19.62785,-19.62159l4.29946,4.29688l-23.9273,23.92426" stroke="null" fill="#2b5797"/></g></svg>`
const SVG_DEVELOPER = `<svg width="16" height="16" viewBox="0 0 64 64"><g><ellipse ry="28" rx="28" cy="32" cx="32" fill="#bc3e3e"/><path d="m49.09975,25.03477l-7.89171,2.18306c-0.72326,-1.92155 -1.81179,-3.5869 -3.12963,-4.91255l4.17361,-7.36662c0.36703,-0.64979 0.14069,-1.47873 -0.51453,-1.84803c-0.64882,-0.36732 -1.47601,-0.13866 -1.84178,0.51023l-3.95929,6.99019c-0.81801,-0.51859 -1.67909,-0.93292 -2.57285,-1.2134c-0.56331,-1.94582 -2.23705,-3.26464 -4.14085,-3.46933c-0.00134,-0.0056 -0.00283,-0.01095 -0.00414,-0.01656c-0.08263,-0.3276 -0.12587,-0.62226 -0.13284,-0.87966c-0.00666,-0.25615 0.01629,-0.47561 0.06248,-0.6489c0.04993,-0.17589 0.10693,-0.31805 0.16928,-0.42915c0.19754,-0.35209 0.46023,-0.58221 0.78129,-0.68919c0.03123,0.38033 0.19236,0.65251 0.47668,0.81359c0.20987,0.11941 0.4549,0.15609 0.7392,0.11697c0.28571,-0.04235 0.50914,-0.20608 0.67192,-0.49193c0.14196,-0.25204 0.1716,-0.52685 0.08649,-0.82182c-0.08649,-0.29651 -0.27484,-0.52546 -0.56055,-0.68654c-0.2086,-0.11755 -0.42527,-0.1777 -0.65255,-0.17505c-0.22627,0.00146 -0.44296,0.04654 -0.65147,0.13182c-0.20718,0.08528 -0.40086,0.21652 -0.5811,0.39091c-0.17872,0.17505 -0.3262,0.36724 -0.44691,0.57692c-0.1786,0.31726 -0.29232,0.67177 -0.33823,1.06063c-0.04472,0.3868 -0.05018,0.76498 -0.01244,1.13759c0.02325,0.21615 0.08263,0.40753 0.12736,0.60773c-0.50778,-0.02846 -1.02637,0.00264 -1.54376,0.14631c-0.65261,0.18035 -1.23607,0.48746 -1.73333,0.88434c-0.07019,-0.2331 -0.13239,-0.46586 -0.24345,-0.70164c-0.15709,-0.33732 -0.35627,-0.65809 -0.59338,-0.96931c-0.24092,-0.3107 -0.51991,-0.55532 -0.83679,-0.73573c-0.20837,-0.11609 -0.43463,-0.20402 -0.67824,-0.2624c-0.24376,-0.05829 -0.47667,-0.07079 -0.69882,-0.03668s-0.43064,0.10837 -0.62684,0.22211c-0.19784,0.11373 -0.35214,0.27747 -0.47136,0.48361c-0.16125,0.28549 -0.20301,0.57964 -0.12735,0.87608c0.0788,0.29914 0.24508,0.5195 0.49711,0.6631c0.28434,0.16115 0.56185,0.18718 0.82737,0.07616c0.26552,-0.11109 0.45624,-0.26923 0.57545,-0.48069c0.16111,-0.28467 0.15979,-0.60008 -0.00962,-0.94092c0.33051,-0.07493 0.67303,-0.01095 1.02502,0.19042c0.10989,0.06102 0.23177,0.15608 0.36576,0.28225c0.1315,0.12439 0.26139,0.30001 0.38725,0.52508c0.12722,0.22483 0.24241,0.49818 0.33985,0.82307c0.0178,0.05984 0.03126,0.13268 0.04754,0.19784c-1.23354,1.12127 -1.83771,2.82749 -1.48691,4.54558c-0.78811,0.79077 -1.45151,1.72014 -1.97306,2.75584l-6.98723,-3.96185c-0.6499,-0.36754 -1.47856,-0.13859 -1.84686,0.51031c-0.36699,0.65276 -0.13819,1.48146 0.51189,1.85077l7.36765,4.17358c-0.4481,1.81435 -0.52407,3.80103 -0.1583,5.81854l-7.892,2.18389c-0.72308,0.19965 -1.14542,0.94716 -0.94493,1.66714c0.19887,0.71796 0.94376,1.14144 1.66686,0.94004l7.96099,-2.20319c0.68102,1.43137 1.84557,3.08759 3.21031,4.66198l-4.32482,7.63636c-0.36843,0.65069 -0.14113,1.47818 0.51166,1.84543c0.32354,0.18569 0.69348,0.21916 1.02666,0.12733c0.3357,-0.0921 0.63486,-0.31007 0.81773,-0.63794l3.92006,-6.91843c3.6562,3.51255 7.69697,5.58361 7.82432,1.42635c2.1638,3.4666 4.67293,-0.45761 6.096,-5.38401l7.04562,3.99023c0.3249,0.18716 0.69612,0.21977 1.03183,0.12767c0.33318,-0.09094 0.63232,-0.3104 0.81637,-0.63796c0.36859,-0.6489 0.14113,-1.47729 -0.51031,-1.84398l-7.71984,-4.3729c0.39396,-2.03762 0.57156,-4.03941 0.43208,-5.60347l7.97015,-2.20321c0.71775,-0.19873 1.14705,-0.94359 0.94239,-1.66357c-0.20034,-0.72058 -0.94786,-1.14321 -1.66815,-0.94448z" fill="#2b5797"/></g></svg>`

function renderList(el, src, pwa) {
  const div = document.createElement('div')
  div.innerHTML = `<div onclick="window.route('app', '?app=${escape(pwa.uuid)}&source=${escape(src.id)}')" style="margin:.65rem;cursor:pointer">
    <img class="appimg" src="${pwa.icon.replaceAll('"', '')}" width="64" height="64" alt="" onerror="this.src='/icons/notfound.png'">
    <div style="margin-left:5rem">
      <span>${pwa.dev ? SVG_DEVELOPER : ''} ${pwa.verified ? SVG_VERIFIED : ''} ${escape(pwa.name)}</span><br>
      <span style="color:#89dceb">${escape(src.name)}</span><br>
      <span style="color:#cba6f7">${escape(pwa.license) ?? 'Proprietary'}</span><br>
    </div>
  </div>`

  el.append(div)
}

async function exploreRoute() {
  const el = $('#explore')
  el.style.display = ''
  const srcs = await db.sources.toArray()
  async function renderSource(json) {
    for (const pwa of json.pwas) {
      renderList(el, json, pwa)
    }
  }
  for (const src of srcs) {
    if (src.cache)
      renderSource(src.cache)
    else
      fetch(src.url).then(x => x.json()).then(renderSource)
  }
}

async function searchRoute(params) {
  const el = $('#search')
  el.style.display = ''
  const srcs = await db.sources.toArray()
  async function renderSource(json) {
    for (const pwa of json.pwas) {
      if (matchSearch(pwa, params.get('search'))) {
        renderList(el, json, pwa)
      }
    }
  }
  for (const src of srcs) {
    fetch(src.url).then(x => x.json()).then(renderSource)
  }
}

async function appRoute(params) {
  const el = $('#app')
  el.style.display = ''
  let src
  if (params.get('srcurl')) {
    src = await (await fetch(params.get('source'))).json()
    src.cache = src
  } else {
    src = await db.sources.get(params.get('source'))
  }
  if (!src) {
    el.innerHTML = `<span>The app's source is not installed and could not be found. Please add the source first.</span>`
    return
  }
  const json = src.cache || await (await fetch(src.url)).json()
  const pwa = json.pwas.filter(x => x.uuid == params.get('app'))[0]
  if (!pwa) {
    el.innerHTML = `<span>The app could not be found in the source. It may have been deleted.</span>`
    return
  }

  window._app_install = async () => {
    if (typeof installMedium === 'function')
      return installMedium(pwa, json)

    const ua = navigator.userAgent.toLowerCase()
    const isAndroid = ua.indexOf("android") > -1

    let medium = installMedium
    if (medium == 'pwa') {
      if (isAndroid)
        medium = 'android'
      else
        medium = 'ios'
    }

    if (medium == 'android') {
      if (pwa.platform?.indexOf('android') >= 0 || confirm('This PWA is not supported on Android. Open anyway?'))
        window.open(pwa.url)
    } else if (medium == 'ios') {
      download(pwa.id + '.pwastore.mobileconfig', (await buildAppProfile(json, pwa)).url())
    }
  }

  el.innerHTML = `<div><a class="back material-symbols-outlined" onclick="history.back()">arrow_back</a><br><br></div>
  <div>
    <img class="appimg" src="${pwa.icon.replaceAll('"', '')}" width="64" height="64" alt="" onerror="this.src='/icons/notfound.png'">
    <div style="margin-left:5rem">
      <span>${pwa.dev ? SVG_DEVELOPER : ''} ${pwa.verified ? SVG_VERIFIED : ''} ${escape(pwa.name)}</span><br>
      <span style="color:#89dceb">${escape(json.name)}</span><br>
      <span style="color:#cba6f7">${escape(pwa.license) ?? 'Proprietary'}</span><br>
    </div>
  </div><br>
  <div>
    <button class="big-btn" onclick="window._app_install()">
      <span class="material-symbols-outlined">add_circle</span>
      Install
    </button>
  </div>
  <div>
    ${pwa.dev ? '<p style="color:#888">This PWA is in development.</p>' : ''}
    ${pwa.verified ?
      '<p style="color:#888">This PWA has full functionality (feature parity with the native app, if any).</p>' :
      '<p style="color:#888">This PWA is missing some features compared to the native app.</p>'
    }
    <p>${escape(pwa.description) || '<i>App has no description</i>'}</p>
  </div>
  <div style="color:#888" ${localStorage.getItem('pwastore_debug') ? '' : 'hidden'}>
    <small style="color:#a6e3a1">ID</small><br>
    <span>${escape(json.id)}.${escape(pwa.id)}</span><br>
    <small style="color:#a6e3a1">URL</small><br>
    <a style="color:inherit" href="${pwa.url.replaceAll('"', "'")}">${escape(pwa.url)}</a><br>
    <small style="color:#a6e3a1">UUID</small><br>
    <span>${escape(pwa.uuid)}</span><br>
    <small style="color:#a6e3a1">Platform</small><br>
    <span>${pwa.platform ? escape(pwa.platform.join(', ')) : 'ios'}</span><br>
  </div>
  `

  if (pwa.url.startsWith('j')) {
    el.innerHTML = `<div><a class="back material-symbols-outlined" onclick="history.back()">arrow_back</a><br><br></div>
    <div>⚠️ This PWA has been detected as malicious and cannot be viewed. If you think this is an error, report it to the PWA's source.</div>`
  }
}

async function settingsRoute() {
  const el = $('#settings')
  el.style.display = ''

  window._stgop_debug = (el) => {
    localStorage.setItem('pwastore_debug', el.checked ? 'yes' : '')
  }
  window._stgop_notrack = (el) => {
    localStorage.setItem('pwastore_notrack', el.checked ? 'yes' : '')
  }
  window._stgop_modeoverride = (val) => {
    localStorage.setItem('pwastore_mode', val)
    installMedium = val
  }

  const l_mode = localStorage.getItem('pwastore_mode')
  el.innerHTML = `
  <h1 style="margin-bottom:0">PWA Store</h1><h3 style="margin-top:0">v1.6</h3>

  <label class="switch">
    <input class="switch-input" id="stg_debug" type="checkbox" ${localStorage.getItem('pwastore_debug') ? 'checked' : ''} onchange="_stgop_debug(this)" />
    <span class="switch-span"></span>
  </label>
  <label for="stg_debug">Show app metadata</label>
  <br><br>

  <label class="switch">
    <input class="switch-input" id="stg_notrack" type="checkbox" ${localStorage.getItem('pwastore_notrack') ? 'checked' : ''} onchange="_stgop_notrack(this)" />
    <span class="switch-span"></span>
  </label>
  <label for="stg_notrack">Disable anonymous analytics</label>
  <br><br>

  <label for="stg_modeoverride">Install mode</label>
  <select id="stg_modeoverride" id="stg_modeoverride" onchange="_stgop_modeoverride(this.value)">
    <option value="pwa" default${l_mode=='pwa'?' selected':''}>Auto</option>
    <option value="ios"${l_mode=='ios'?' selected':''}>iOS</option>
    <option value="android"${l_mode=='android'?' selected':''}>Android</option>
  </select>
  <br><br>
  `
}

async function sourcesRoute(params) {
  const el = $('#sources')
  el.style.display = ''

  let toaddjson = null, toaddsrc = null

  window._stgop_addsource = async (src) => {
    if (!isurl(src))
      return
    const json = await (await fetch(src)).json()
    if (json && json.id && json.name && json.pwas) {
      toaddjson = json
      toaddsrc = src

      // 99.9% sure this is a source and not random data
      $('#src-add-confirm > img').src = json.icon
      $('#src-add-confirm > div > b > span').textContent = json.name
      $('#src-add-confirm > div > span').textContent = src
      $('#src-add-confirm').style.display = 'block'
    }
  }
  window._stgop_finishaddsource = async (el) => {
    const json = toaddjson
    const src = toaddsrc

    await db.sources.put({ id: json.id, icon: json.icon, name: json.name, url: src, cache: json })
    window.route('sources', '?sources=1')
    el.parentNode.parentNode.style.display = 'none'
  }
  window._stgop_deletesource = async (srcid) => {
    if (!confirm('Are you sure you want to delete this source?'))
      return
    await db.sources.delete(srcid)
    window.route('sources', '?sources=1')
  }
  window._stgop_refreshsource = async (el, src) => {
    el.textContent = 'sync'
    try {
      const json = await (await fetch(src)).json()
      await db.sources.put({ id: json.id, icon: json.icon, name: json.name, url: src, cache: json })
      el.textContent = 'published_with_changes'
    } catch (e) {
      console.error(e)
      el.textContent = 'sync_problem'
    }
  }

  const srcs = await db.sources.toArray()
  el.innerHTML = `
  <button class="big-btn" onclick="_stgop_addsource(prompt('Add Source from URL'))">
    <span class="material-symbols-outlined">add_circle</span>
    Add Source
  </button>
  <br>

  <div id="src-add-confirm" style="display:none;margin-bottom:2rem;">
    <img src="/icons/apple-touch-icon.png" alt="" width="64" height="64" class="appimg">
    <div style="margin-left:5rem">
      <small style="display:block;font-variant:small-caps">add new source</small>
      <b><span>Official Trusted Source</span></b><br>
      <span>url</span><br>
      <span style="cursor:pointer" class="material-symbols-outlined" onclick="_stgop_finishaddsource(this)">
        add_circle
      </span>
      <span style="cursor:pointer" class="material-symbols-outlined" onclick="this.parentNode.parentNode.style.display='none'">
        close
      </span>
    </div>
    <br>
    <hr>
  </div>

  ${srcs.map((src) => `<div style="display:block;margin-bottom:2rem">
    <img src="${encodeURI(src.icon)}" alt="" width="64" height="64" class="appimg">
    <div style="margin-left:5rem">
      <span>${escape(src.name)}</span> <span style="color:#888">(${escape(src.id)})</span><br>
      <span style="cursor:pointer" class="material-symbols-outlined" onclick="_stgop_refreshsource(this,'${src.url.replaceAll("'", '"').replaceAll('"', '`')}')">refresh</span>
      <span style="cursor:pointer" class="material-symbols-outlined" onclick="_stgop_deletesource('${src.id.replaceAll("'", '"').replaceAll('"', '`')}')">delete</span>
    </div>
  </div>`).join('')}
  `

  if (params.get('add')) {
    window._stgop_addsource(decodeURIComponent(params.get('add')))
  }
}


$('#searchform').addEventListener('submit', async ev => {
  ev.preventDefault()
  const search = $('#searchbox').value
  if (search.trim() == '') {
    window.route('explore', '?')
    return
  }

  window.route('search', '?search=' + encodeURIComponent(search))
})

window.route = (_, uri) => {
  history.pushState(null, null, uri)

  doroute(new URLSearchParams(uri))
}

function doroute(params) {
  for (const el of document.querySelectorAll('section')) {
    el.style.display = 'none'
    el.textContent = ''
  }

  if (params.get('app')) {
    appRoute(params)
  } else if (params.get('search')) {
    searchRoute(params)
  } else if (params.get('settings')) {
    settingsRoute(params)
  } else if (params.get('sources')) {
    sourcesRoute(params)
  } else {
    exploreRoute(params)
  }
}

doroute(new URLSearchParams(location.search))

window.addEventListener('load', async () => {
  if ('serviceWorker' in navigator)
    navigator.serviceWorker.register('/sw.js')
  if (!localStorage.getItem('pwastore_firsttime')) {
    const json = await (await fetch(location.origin + '/core.json')).json()

    await db.sources.put({ id: 'app.pwastore.core', icon: '/icons/apple-touch-icon.png', name: 'Official Trusted Source', url: location.origin + '/core.json', cache: json })
    window.route('explore', '?')
    localStorage.setItem('pwastore_firsttime', true)
    $('#first-time-dialog').showModal()
  }
})
window.addEventListener('popstate', () => {
  doroute(new URLSearchParams(location.search))
})

async function installWithNative(pwa, src) {
  this.postMessage({
    huevos: {
      native: {
        install: {
          id: src.id + '.' + pwa.id,
          name: pwa.name,
          url: pwa.url,
          type: 'webapp',
          icon: await fetchIcon(pwa.icon)
        }
      }
    }
  })
}

// Support for HuevOS Extensions
window.addEventListener('message', ev => {
  if (ev.data.huevos && ev.data.huevos.extensions) {
    let native = ev.data.huevos.extensions.native
    if (native) {
      if (!native.includes('install')) {
        // request install permission if not already granted
        console.log('[Native] requesting install permission')
        ev.source.postMessage({ huevos: { permissionrequest: [ 'install' ] } })
      } else {
        console.log('[Native] install medium enabled')
        if (installMedium == 'pwa') installMedium = installWithNative.bind(ev.source)
      }
    }
  } else if (ev.data.huevos && ev.data.huevos.permission) {
    if (ev.data.huevos.permission.includes('install')) {
      console.log('[Native] install medium enabled')
      if (installMedium == 'pwa') installMedium = installWithNative.bind(ev.source)
    }
  } else {
    console.warn('unknown message from ' + ev.origin, ev.data)
  }
})
