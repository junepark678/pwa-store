
# PWA Store
The iOS app store for PWAs.

## Installation
To install PWA Store, just open https://pwastore.vercel.app/ and follow the popup to install it.

When you install a PWA, a profile will be downloaded. Go to `Settings App > General > VPN & Device Management` then click on the downloaded profile and install it. Profiles are generated in-device and only contain the Web Clip required to install the PWA.

## Android support
Currently, Android is only supported for PWAs that can be fully installed as standalone apps. To install them, just press the three dots and then press *Install app* or *Add to homescreen*.

## Thirdparty
This repo hosts some third-party icons. These icons are under their respective licenses and we use them under their guidelines.
**If your icon is being used in this repo and you want it gone, please oppen an issue or send us an [email](mailto:contact@jdevstudios.es).**

## Trusted Sources (WIP)
By default, the only source is the Official Trusted Source. Users can add their own sources by URL to the app. The sources tab also contains a list of Trusted Sources that users can add. **Trusted Sources** are monitored by PWA Store developers to make sure they meet the [Trusted Source Guidelines](./TRUSTGUIDELINES.md).

## PWA Store API
PWA Store provides various APIs to help PWA developers integrate their app into PWA Store.

### Buttons
Check the `btn/` directory for premade button images.


**Add Source** buttons can be done by redirecting the user to the following URL:
`https://pwastore.vercel.app/btn/source?url=URICOMPONENT-ENCODED-SOURCE-URL`


**Get it on PWA Store** buttons can be done by redirecting the user to the following URL:
`https://pwastore.vercel.app/btn/app?id=APP-UUID-HERE&source=SOURCE-URL-HERE`


**Direct Install** buttons can be done by redirecting the user to the following URL:
`https://pwastore.vercel.app/btn/install?id=APP-UUID-HERE&source=SOURCE-URL-HERE&redirect=URICOMPONENT-ENCODED-REDIRECT-URL`

### Storefront (WIP)
You can make your app look better on PWA Store by adding some properties. Apps with Storefront properties are customized and attract more users.


Storefront properties for apps:
```json
{
  "storefront": {
    "background": "#b4befe",
    "screenshots": [
      "https://example.com/screenshot/001.png",
      "https://example.com/screenshot/002.png",
      "https://example.com/screenshot/003.png"
    ],
    "banner": "https://example.com/banner.png"
  }
}
```

> Colors should follow the [Catppuccin Mocha](https://github.com/catppuccin/catppuccin#-palette) color palette to look best. Keep in mind you should keep considerable contrast so text is readable!

> Screenshots should be concise and clear. Don't include more than 3 screenshots. Screenshots will be cropped unless the user presses on them.

> Banners should have good contrast with the app icon and text.

### Advertisements (WIP)
You can advertise your app on PWA Store by adding some special properties to your source and app.
If you enroll your app on Advertisements, it will get promoted based on user activity. Sources must be trusted *or* installed in PWA Store to be advertised.


Artificially inflating your statistics will get your app banned from Advertisements.
You must follow the [Trusted Source Guidelines](./TRUSTGUIDELINES.md) to enroll in Advertisements.


To enroll your **source** on Advertisements, add the following properties:
```json
{
  "advertisements": {
    "promote": true,
    "banner": "https://example.com/ads_banner.png",
    "categories": [ "social", "fun" ]
  }
}
```

Then, to enroll **apps from your source**, add the following properties:
```json
{
  "advertisements": {
    "category": "social",
    "short": "The best app for sharing cat pictures!"
  }
}
```

> Available categories are `tech`, `games`, `tools`, `fun`, `social`, `finance` and `other`.

### Analytics (WIP)
All app installs are tracked anonymously unless the user disables it.
Anyone can request the API for analytics data.
Inflating the app install numbers *is* technically possible, but our systems will flag suspicious activity and reflect it on the analytics results.


To request analytics information about your app, do a GET request to `https://pwastore.vercel.app/api/analytics?id=ANID-HASH-HERE`. The ANID hash is the hexadecimal representation of the SHA256 hash of `id:uuid`.
